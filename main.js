'use strict'
const express = require('express')
const initPayload = require('./foo')
const app = express()
const payload = initPayload()
app.get('/', (req, res) => {
  res.send(payload())
})
// app.listen(3000)
